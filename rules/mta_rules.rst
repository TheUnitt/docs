################
Multi Theft Auto
################

************
Server Rules
************
.. note::

  The Owlgaming Server Rules are all the rules that are related to accounts, scripts which aren't monitoring in character actions and more.

.. _bug tracker: https://bugs.owlgaming.net/
.. _UAT Contact: https://forums.owlgaming.net/forms/10-upper-administration-contact-ooc/
.. _Support Center: https://owlgaming.net/support/

Transferring Assets
===================
Transferring assets is strictly limited to premium users. If you wish to transfer money, vehicles, property or other assets, purchasing the perk with gamecoins is mandatory. You are not allowed to transfer weaponry of any sort. You will also not be given back any impounded or seized vehicles after a transfer.

Scams
=====
Scamming of vehicles and properties is not allowed by any means.

AFKing
======
Leaving your computer or in any manner tabbing out or going AFK should not be done in a public area, please use our F10 option going to the character menu for this.

Inactivity
==========
During inactivity, your assets will be force-sold. Assets only being your interiors and vehicles. Money and items on your characters will not be removed during inactivity. If an asset is force-sold with this reason, it is not refundable. An asset, of any type, is considered inactive after having not been entered in 14 days or the owner has been inactive for 30 days. Only one of the two is needed.  

Interiors
---------
Interiors can be force-sold including all the side interiors linked to the property. If a house is inactive, the garage will be force-sold along with it. This is to keep the properties as one. The main property always overrules the sub-properties. So if a garage or bedroom is inactive but the house itself isn't, the house and all subinteriors (including the inactive one) cannot be force-sold.

Interiors may be force sold by admins if the owner is avoiding the inactivity scanner without the use of inactivity protection by logging on and using the interior or having a friend use the interior without roleplaying on the server with that character.

Vehicles
--------
Vehicles will never be force-sold, unless it has been parked in a force-sold interior and you have been inactive for 30 days or it has not been used for 14 days. If you purchase an interior that was force-sold and vehicles still exist in the interior, they will be deleted if inactive or they will be taken as parked there in character. When being impounded you also have 14 days to release your vehicle, else the respective faction may claim your vehicle and sell it to another player. This regardless if you're inactive or not.


Retexturing
-----------
Retexturing the game has been made possible scriptwise, yet has its rules in order to maintain a certain level of decency. Erotically oriented pictures may only be used in appropriate/private spaces i.e. personal interiors or strip clubs. Same applies for any socially unacceptable pictures, they are to be kept out of interiors accessible for the public. 

Billboard texturing can only be done for official legal factions to promote themselves (reserved for government and it's municipal agencies). Exterior re-texturing can be placed to advertise a business if permission is given through a `UAT Contact`_.

 
**************
Roleplay Rules
**************
.. note::

  These are our core rules which are applied universally while roleplaying.

.. _UA: https://forums.owlgaming.net/forms/10-upper-administration-contact-ooc/


Always Roleplay
===============
All players must remain in character at all times unless allowed to go out of character by an administrator or they are themselves an administrator.

*"Roleplay now,* `report <https://owlgaming.net/support/>`_ *later."*

Brawls
------
Standard procedure is to fight in an RP manner, however, players can OOCly agree to perform a fight in a brawl done with GTA physics.

Vehicular Pursuits
------------------
An exception of always roleplay is doing a PIT maneuver during a chase.

MetaGaming
==========
MetaGaming is when someone uses out of character (OOC) information for in character (IC) purposes. If you attempt to
incite MetaGaming, then you're also breaking rules.

PowerGaming
===========
A player can be described as a PowerGamer if he or she presumes or declares that his or her own action against another player
character is successful without giving the other player character the freedom to act on his or her own decisions. This includes
but is not limited to:

* Forcing actions upon a player.
* Failing to allow a player to roleplay their own actions.
* Use of items which you haven't physically obtained.
* Acting superhuman.

Character Development
-----------------------
Character development plays a vital role in what is considered PowerGaming. A character that goes to the gym and works out regularly
may be in better physical condition than others involved in the situation.

Special Characters
^^^^^^^^^^^^^^^^^^
Special Characters are characters which have a particular subset of skills (superior strength, shooting, stamina, etc), such as but not
limited to:

* Mentally Challenged
* Skilled Martial Artists
* Members of Special Operations Forces

These characters must have `UA`_ approval prior to roleplaying as such.

Vehicles
----------
Vehicles which are used as they are not designed, such a lowrider offroading is considered PowerGaming as the vehicle would not be
able to sustain such conditions without breaking or becoming inoperable.

Deathmatching
=============
Deathmatching is the act of killing another persons character without sufficient reason or proper roleplay.

Death
============

Player Kills
------------
A player kill is when your character is killed, simulating unconsciousness and amnesia which extends as far back as that particular roleplay situation's beginning.

**Example**

  John Smith goes to a bar in a bad part of town and meets a particularly violent drunk named Wilson LaRoche who while minding his own     business, hits his girlfriend a few times. John Smith, being a white knight of the situation, tries to intervene. The two get into a     physical altercation and while Wilson is inebriated he loses some self control, kicking John Smith repeatedly in the face after he       collapsed against an arcade machine. Slumped in the corner, John Smith is player killed where he his health is depleted and he is       killed script-wise.

No application is needed to player kill someone. Only a solid in character reason.

Roleplaying After a Player Kill
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If a character is player killed and the scene is left after they respawn, the players involved should roleplay that the unconscious character is found by a good Samaritan, emergency services were called and they were taken to the hospital. The person that was player killed would then after they respawn at the hospital, roleplay in the hospital for a period of time for their injuries to heal and continue to roleplay those injuries accordingly.

Do not respawn and immediately run around as if nothing happened!

Character Kills
---------------
A character kill is when your character is permanently killed and the ability to access that character is disabled via script function. They serve as a means of permanently ending a character's life and their story. Character kills should not be taken lightly. Remember, a majority of situations can be dealt with before resorting to a character kill by beating, player kill, etc.

**Example**

  John Smith over time due to his addictive personality accumulated gambling debt from a local underground poker establishment. Weeks pass     and John Smith fails to make consistent payments on the gambling debt, totaling $50,000. He is threatened and beat up but continues     failing to pay his debt. The poker facility manager and crime boss decides to kill John Smith as retribution for not paying back the     money and to send a message to the other people that owe him money that they should pay in a timely manner.

  A sum of $50,000 is a substantial amount of money. If a smaller amount is owed, say, $10,000, a severe beating may be in order and perhaps the   crime boss' goons break into his house and steal stuff from him to get some payment and a loan would never be given to John Smith       again because $10,000 would likely not warrant something as severe as murder.

An `application <https://forums.owlgaming.net/forms/20-general-administration-character-kill-appeal-ooc/>`_ is necessary to character kill someone due to the severity of it. If your character kill application is sensitive, you may send make a `private <https://forums.owlgaming.net/forms/8-senior-administration-private-character-kill-application-ooc/>`_ request which is sent to our senior administrators. Private character kills may be sent to upper administration members as well for the most privacy. Additionally, character kills may be accepted in game by a single administrator for situations where you require a quick response where you otherwise could not wait for an application.

In order for a character kill to be valid, the character being killed should generally be killed script wise. There are a few types of exceptions to this. If for example someone's foot is exposed and it is shot over and over and they die script wise, they would not necessarily realistically die. Thus, if they received reasonable medical treatment before they bled out, they'd survive and lose their foot. On the other hand, if that person hadn't received prompt medical attention, they would bleed out and die, warranting a character kill even if they hadn't died scriptwise. Another example of this would be if someone is run over by a vehicle. With the game physics, they may not lose very much health, but in reality, they would very easily be killed at a high speed impact.

Character Kill Scenarios
^^^^^^^^^^^^^^^^^^^^^^^^
A character kill scenario is exactly as it sounds, a scenario of serious roleplay where character kills are issued to characters that die. These are common in roleplay events and large or serious situations.

In order for a character kill scenario to be valid, there must be proof. An administrator must witness the situation or there must be roleplay logs, a video, credible eye witnesses or comprehensive screen shots then shown to the handling administrator. Character kill scenarios may happen at any time an administrator deems necessary, thus, players should always be realistic and careful as they would in real life.

A character kill scenario may not necessarily be publicly mentioned to people involved to prevent metagaming where people shoot just to get a character kill where they otherwise wouldn't have shot if they didn't know them shooting someone was a guaranteed character kill on another player.

Below are some brief examples of character kill scenarios. They are by no means a comprehensive list showing the only situations you may be character killed but instead serve to give you a better idea of what are the most common scenarios.

Disregard for Life
""""""""""""""""""
Someone showing disregard for their own life or unnecessary risk such as acting suicidally or     generally not caring for their actions as carefully as they would in real life. Some more detailed examples of this would be:

  * Pulling a gun out on a police officer during a traffic stop when you are going to receive a traffic infraction, your character is then killed.

  * Reckless handling of a vehicle or aircraft and crashing it which would clearly result in death, especially at a very high rate of speed.

  * Police acting like they’re invincible, failing to follow proper safety protocol, acting out unrealistically in situations where they normally wouldn’t.
  
Please note, not all forms of disregard must or even should result in a character kill. The circumstances of the scenario and context matter greatly. If someone is resisting capture or kidnapping against 4 people, their chances of actually escaping and telling the police is slim to none which means if one of those 4 kidnappers is a hothead and shoots the person resisting capture, it should not be immediately deemed a character kill. The weight of their actions should be taken into account.

Organized Robberies
"""""""""""""""""""
Robberies which are organized, whether spur of the moment or heavily pre-planned are common circumstances in which a player (the robber) may be character killed. This includes heists, ammunation robberies, quick 24/7 robberies, etc.

  * Your character is being robbed by someone who is wielding a gun, rather than give up the $100 you have in your wallet, you pull your own gun out and attempt to shoot the robber. The robber then shoots and kills you.

  * A car of gang members burst into a convenient store to rob it. Inside is someone carrying a gun. Upon seeing the men aim a gun at the cashier, they draw their gun and manage to shoot two of the robbers and kill them.

Furthermore, just like with disregard for life, not all robbery related deaths have to be character kills. If robbers jump out of a van and aim guns at a woman on the side of the street, it is a normal, instinctive reaction for someone to immediately run at the first sight of a gun. If that woman is then shot in the back, her death wouldn't be considered a character kill. If she is, however, cornered in an allyway for a moment with guns to her face and she tries to run or escape or physically resist, she's had some time to think about the scenario and running isn't a purely instinctual response at that point, thus, her death could be a character kill. (Though again, it could just as easily be a player kill too! Those do have severe consequences!)

Planned or Supervised Events
""""""""""""""""""""""""""""
Some events such as large fires, catastrophic weather, terrorist attacks, freak accidents, etc, are pre-planned or setup and supervised by admins. Some naturally occurring events become these types of large situations such as a large gang shootout, large pile-up car accident, and so on. During these types of events players may be eligible for a character kill as well.

  * A plane crashes into a building where the fuel catches on fire and you are caught in the blaze.

  * A landslide beside a mountain which crushes you as a civil servant or simply there as a civilian.

  * A man hunt for a criminal within a certain area who is knowingly armed and dangerous.

Roleplaying Death
"""""""""""""""""
When any player roleplays death it may result in a character kill. A player cannot kill themselves or roleplay death and then have it appealed to come back, or try to roleplay coming back to life (unless assisted by CPR or defibrillators) within reasonable limits.

Infiltrating Organizations
""""""""""""""""""""""""""
Characters that infiltrate an organization to gain intelligence, insiders knowledge, evidence, or any other means of information that could harm the survival of an organization. This clause focuses primarily on law enforcement infiltrating criminal organizations, but is applicable the other way around or with criminal organizations infiltrating each other as well.

  * A detective joining a street gang to help police take them down. One of the gang members finds out and murders the detective.

  * A lawyer joining a law firm to plot and overthrow the organization, steal accounts, clients, etc. One of the scumbag lawyers involved hires a bum to stab the infiltrator in a spur of the moment idea just after a big argument.

  * A criminal or informant joining the police department to gain knowledge for a gang. A detective with corruption that finds out they have been deceived in a blind rage one night kills the informant.

Police Situations
"""""""""""""""""
A brief forewarning, not all shootings or scenarios need to be character kills. Just because a character kill may happen does not mmean it has to happen. Admins are encouraged to treat more shootings and situations as player kills and be more strict about character kills because it will generate more roleplay for everyone involved. 

If a situation arises where the death of a police officer is justified by the hands of (a) criminal(s) due to serious ramifications that the player would suffer if caught, a character kill may be enforced for both parties. Police officers as a whole though are not character killed if they are simply doing their job and character kills on police officers are also not justified if you are utilizing violence on a police officer to prevent yourself from being caught for less substantial crimes such as robbery, battery, drug dealing, weapon possession, etc. Players who escalate the level of force used against police officers cannot use that escalation to justify a character kill. An example of this will be below.

  * A criminal is fleeing police custody for a drug charge. If they shoot at police to escape this, it wouldn't be a character kill because the punishment they are fleeing from is not essentially life threatening like life in prison would be for murder. If the criminal fleeing police for a drug charge shoots at the police, even though this was escalated to potentially attempted murder, a character kill wouldn't be valid because the escalation was done by the fleeing party. Forcing a character kill on the officer at this point would mean that the vast majority of police deaths like this are character kills which is unsustainable.

  * A criminal has an arrest warrant out on them for murder which would result in life in prison. The player shoots at the police officer to prevent the officer from identifying and arresting them so they may escape. The police officer may be killed in this situation where substantial stakes are at risk. Foreknowledge is a mitigating factor in this circumstance. If the officer did not know there was an arrest warrant for murder, the chances of them being character killed are reduced. If they did know they had an arrest warrant on them for murder, the chances are increased.

  * During a gun deal where a high-level gang leader is involved a police officer pulls up on them. In order to protect the identity of the high-level gang leader and prevent an investigation which could cripple the gang, everyone flees and a few of the gang members at the deal shoot at the police officer, killing them.

Character Kill Clauses
^^^^^^^^^^^^^^^^^^^^^^
Factions may have a character kill clause that you inherently take upon yourself by associating with them. These factions are generally illegal ones. In order for a character kill clause to be valid it must be present on their thread and submitted to the Faction Team so they are aware of it and can validate your claim of the clause in the future to ensure it is not being made up.

Generally character kill clauses for factions cover anyone who is an associate and above. The criteria for someone to be character killed is nearly endless and is generally approved by a leader of the faction. This is the inherent risk in being part of illegal roleplay. You are considered an “associate” and above if you willingly take part in illegal activity with an associate or member of the faction.

Extreme or Disgusting Roleplay
===============================

Consent
-------
Every party involved, including witnesses, must OOCly agree to participate in any of the situations listed below:

* Rape
* Cannibalism
* Bestiality
* Necrophilia
* Sexual Harassment

You may withdraw your consent at anytime during the roleplay.

Prohibited
----------
Roleplay in the following list is prohibited in any circumstance:

* Sexual roleplay of minors (younger than 16)

Roleplay Binds
===============
Binds to draw or holster one handed weapons are allowed as they naturally have a faster draw time. Two handed weapons such as assault rifles, rifles, shotguns, etc. require a manually typed out /me to draw the weapon, unless it is easily accessible due to predetermined RP (gun racks, gun slings, gun on lap, etc).

Logging to Avoid
================
Players are forbidden from logging out during a roleplay unless approved by an administrator. Do  not join in a large roleplay situation if you cannot commit the time.

Law Enforcement Situations
--------------------------
After criminal activity in which Law Enforcement may become involved, you must wait 30 minutes prior to logging off.

Provoking
==========
Seeking attention from law enforcement or emergency services by shouting at them, making 911 calls to be chased, etc, is prohibited.

Evidence
=========
All actions may leave traces left behind from the roleplay. Such as, but not limited to:

* CCTV Footage
* Finger Prints
* Tire Treads or Shoe Imprints
* Broken Locks / Doors
* Glass Fragments
* Civilian Witnesses
* Etcetera

Notes should be dropped indicating this evidence and information must be given to any overseeing administrators so they may relay the information to investigative parties.

Vehicle Descriptions
====================
Vehicle descriptions via /ed should be used to present the physical features of the car, not internal specifications or information which cannot be readily seen from the outside.

CCTV Cameras
============
CCTV Cameras are by default, roleplayed as a 90 degree angle camera with 480p resolution at 5 frames per second. The data must be stored somewhere when roleplaying the install. All CCTV camera installations/upgrades must be approved by an administrator and added to the interior note. Footage is wiped at the end of the week if nothing of significance has occurred unless otherwise specified.

All government buildings and gas stations are assumed to have sufficient cameras to cover most common angles both inside and outside.

Roleplay Zone
=============
All roleplay must be done within a confined zone known as "`Los Santos County <https://imgkk.com/i/44da.jpg>`_". There are exceptions for dynamic situations such as car chases which may naturally lead outside of the roleplay zone. Additional exceptions may be specific ones approved by the Upper Administration Team such as the drag strip in Las Venturas.

**************
Legal RP Rules
**************
.. note::

  These are the legal roleplay rules in place to maintain order and stability in the server.

.. _UA: https://forums.owlgaming.net/forms/10-upper-administration-contact-ooc/

Gates
===============
Gates may only be placed on property that you own. If you do not own the property, the owner of the property must grant you permission to place a gate on the property. In order to request a gate, contact the Mapping Team. You must provide logs of gate installation after the Mapping Team accepts your gate. 

Generic Items
=============
In general, generic items that adversely affect others during roleplay situations must be present such as gloves to prevent fingerprints, a bat to bludgeon someone, a CCTV camera to provide surveillance, etc. Generic items which do not adversely affect others in general, are not required, like a toolbox you pull a hammer out of.

All legally obtainable generics must be purchased in character through shops or the appropriate faction providing delivery services. Minor exceptions may be made on a case-by-case basis for spawning legal items under specific situations which are time sensitive such as needing a tire iron to change a tire when it is otherwise completely unavailable or where an item would reasonably be present and is necessary to roleplay like breaking into a police car and stealing the computer inside.

If one decides to roleplay having jewelry of significance, they need to have a generic made for it. This is the case for expensive watches, rings, and other luxurious jewelry. A simple wrist watch does not need a generic made for it.

Exceptions for items which are not rare and commonly available at big box stores may be made by the handling administrator at their own discretion.

Generic items are not to be used in world 0 to replace exterior world mapping.

Items which have an illegal use and thus the purchase would not be registered or taxed, should be acquired through illegal factions in the server or a request to the Faction Team. 

Interiors
=========
When purchasing an interior you must also buy the associated interiors that come with it. An example being buying a house, and having to buy the garage that comes with it. You may not buy a garage and then not buy the house. If you do this, you will have the interior removed from you.

If an interior is disabled via means of roleplay such as a fire, natural disaster, etc, it must be renovated to be enabled again. Roleplay must be provided to an administrator and documented in the interiors history.

Weapon Licenses
===============
Weapon licenses may be attained in character by filing the required paperwork found on the Los Santos Police Department website. 

Legally purchased weapons and ammunition may not be sold on the black market or to friends illegally. Legally purchased weapons may also not be used for criminal purposes.

It is prohibited to stat transfer weapons. If this rule is broken, punishment similar to alt to alt will occur.

Tier 1 and 2 licenses availbiites and restrictions are dependent on the in character laws at the time. A Tier 3 license must be requested from the Faction Team.

NPCs & Shops
===============
To uphold realism and to ensure RP, the rules in place must be followed on the subject of NPCs.
 
You may not have an NPC that serves as a guard for security unless:

- The NPC is on government property (cityhall, SD, courts, etc.

- UAT permission is given with exigent circumstances
 
If you wish to have security, you will need to hire it ICly through role playing. This will promote legal RP and aim to keep situations where NPCs will be used to deter crime to a minimum as it often brings conflict.
 
- The Rapid Auto Parts - Viozy NPC may not be spawned outside of RTs lobby.

- The Prison Worker NPC may not be spawned outside of a prison.

- The generic “NPC” may not be spawned outside of government property (exceptions are to guard exterior doors, fences, etc)

- The One Stop Mod Shop NPC may not be spawned outside of its Blueberry Xoomer location.

- The Santa Grotto NPC may not be spawned unless UAT permission is given.

- Faction NPC and Weapon NPCs may not be spawned without UAT and or FT permission.

- You may not have any store, custom or default, in residential or mechanical interiors.

- You may not have both an electronics store and a general store NPC in the same interior unless you own a “superstore” class interior.
 
Exempt from store NPC limitations are malls, example being JGC Mall, where the interior price is $5,000 but the building would be worth much more. In situations as these, use common sense. 

All stores must be set as "business" type ID.
 
Store Classes and Limitations
-----------------------------

**General Stores (24/7, corner stores, small shops for items found in these businesses)**

If property price $10,000 - $35,000:

- 1 NPC for general store items

- 1 Custom NPC

If property price is  $35,000 - $50,000:

- 2 NPCs for general store items
- 2 Custom NPCs

If property price is $50,000

- 3 NPCs for general store items
- 3 Custom NPCs

**Gun and ammunition stores (ammunation, etc)**

If property price is $10,000 - $35,000:

- 1 NPC for guns and ammunition
- 1 Custom NPC
 
If property price is $35,000 - $50,000:

- 2 NPCs for guns and ammunition
- 2 Custom NPCs
 
**Food stores (Food, alcohol, cafes)**

If property price is <$35,000:

- 1 NPC for food and drink items
- 1 Custom NPC
 
If property price is ≤ $50,000:

- 2 NPCs for food and drink items
- 2 Custom NPCs

**Sex stores (sex shops only)**

If property price is <$35,000:

- 1 NPC for sex store and clothing store each
- 1 Custom NPC

If property price is ≤ $50,000

- 2 NPCs for sex stores and clothing stores each
- 2 Custom NPCs

**Clothes stores (clothes, designers, etc)**

If property price is <$35,000:

- 1 NPC for clothes
- 1 Custom NPC

If property price is ≤ $50,000

- 2 NPCs for clothes
- 2 Custom NPCs
- Gyms (gym only)

If property price is <$35,000:

- 1 NPC for gym
- 1 Custom NPC

**Electronic Stores (electronics)**

If property price is <$35,000:

- 1 NPC for electronics
- 1 Custom NPC

If property price is ≤ $50,000

- 2 NPCs for electronics
- 2 Custom NPCs

**Book Stores (book stores, libraries, dictionary shops, etc)**

If property price is <$35,000:

- 1 NPC for book stores
- 1 Custom NPC

If property price is ≤ $50,000

- 2 NPCs for book stores
- 2 Custom NPCs

**Hardware Stores (hardware tools, renovation centers, etc)**

If property price is <$35,000:

- 1 NPC for hardware store
- 1 Custom NPC

If property price is ≤ $50,000

- 2 NPCs for hardware store
- 2 Custom NPCs

**Superstore (Minimum property price of $105,000)**

- Any 5 NPCs
- Unlimited custom NPCs


**************
Criminal Rules
**************
.. note::
 The OwlGaming Criminal Roleplay Rules are set up due to the high amount of criminal roleplayers and in order to set limitations and somehow create a market. Money should flow and allowing only certain people to do specific actions can ensure that. We also aim to let people venture in new kinds of roleplay, which they didn't do before, sadly this has to be done by setting limitations, sometimes.

Restricted Areas
================
You may not commit serious crimes on purpose in high profile areas without adequate law enforcement on duty. You may check if there are enough law enforcement by asking an administrator. This limitation, however, is not to be used as a "safe zone". If you are being chased by an attacker and run onto the steps of a police station, this does not mean they must suddenly stop all illegal activity. The following zones include all of their reasonable surrounding perimiters.

All restricted areas may have crimes purposefully committed on them such as robberies of banks with permission from the UAT.

Examples:

* Government Buildings such as County Hall, Memorials, Court Houses, Court Offices, and similar Facilities
* All Saints General Hospital, County General Hospital, or similar Medical Facilities
* Los Santos Police Department HQ and Precincts and Facilities
* Los Santos Fire Department HQ & Facilities 
* San Andreas Detention Center
* Bank of Los Santos 


Arson
=====
Comitting arson requires that an adequate number of emergency service employees are available to respond to the fire and must be approved by any administrator. All arson requires the owner of the property to be online when the request is made to administrators.

Note: It is assumed that all gas stations and government buildings have fire prevention systems.

Small Fires
-----------
Vehicles, small buildings without anyone inside, sheds, and similar places may be targeted with arson provided that there are at least two firefighters available.

Large Fires
-----------
Large buildings, buildings with people inside, forest fires, and similar places may be targeted with arson provided there are at least five firefighters available.

Kidnapping
===========
If planning to leave a character to starve to death, one must have a CK application accepted on the character. Otherwise there must be roleplay intended to free/feed the character.

Property Break-Ins & Robbery & Theft
====================================
If administrative intervention is necessary to complete a robbery or theft, such as unlocking a door that was kicked in, it must adhere to the following rules and needs administrative approval. If a door was left open for example and you just walked in, you do not need to follow the rules below or approval. An administrator is needed if property break-ins are done through the use of a door ram and you are not law enforcement.

You may not rob / steal:

* If you have less than 10 hours on your character.
* Other characters with less than 5 hours.
* On-duty law enforcement officer's equipment without administraive permission.
* Faction badges or identification cards.
* Automated teller machine cards (unless the player agrees).
* Private custom skins (unless the player agrees).
* More than $3,000 from someone's bank account utilizing their stolen automated teller card (unless the player agrees).
* Property or vehicle keys (unless the player agrees).
  
 Some pieces of property require Upper Administration Approval to break into. They are:

* Evidence Storage
* Ammunations
* Banks

Residential Property
--------------------
All private residential property may be broken into and burglarized at any time except when the owner of the property is logged out inside the interior. When the request is made at least two law enforcement officers must be available to respond.

Commercial Property
-------------------
Commercial property such as storefronts, offices, businesses, etc, may be broken into and burgalarized. At least two law enforcement must be available to respond.

Faction & Government Property
-----------------------------
Since there is no owner for faction or government properties, only the requisite amount of law enforcement must be available.

Warrants & Property Inspections
-------------------------------
Law enforcement and emergency services personnel may enter properties without the owner needing to be online with a sufficient enough reason such as serving a search or arrest warrant, entering the property with exigent circumstances, performing a fire inspection, etc.

Safes
-----
Unless otherwise specified, all safes in interiors are to be roleplayed as mid-tier household safes.
  
Character Robberies
-------------------
You may not player kill someone just before, during, or directly after a petty robbery. This is to prevent abuse of the amnesia effect when normally violence would not be utilized. The exception to this is if someone shows disregard for their life or there is some other exigent circumstance such as police showing up and you shoot at them to prevent them tackling you if you try to escape.
  
Vehicle Robbery
---------------
Vehicle robbery means vehicles which are broken into and have the contents inside stolen. They do not require the owner to be online and are treated like faction or government property robberies, however, government vehicles require permission from an administrator to be broken into.

Vehicle Theft
-------------
Vehicle theft refers to actually taking and moving the vehicle in question. This may only be done or attempted once every 24 hours. Just like robbery, government vehicles require permission from an administrator. An exception to this is a spontaneous vehicle pursuit and the government vehicle in question is co-opted.

If the key is in the inventory of a vehicle, you may steal the car without admin permission. If the player timed out you may not steal the vehicle, an admin can check connection logs to determine if the player timed out.


*************
Faction Rules
*************
.. note::

  Further information regarding factions such as critera for becoming official or making requests may be found in the Faction Information thread.
  
Faction Wars
============

Starting & Ending a War
-----------------------
If a feud occurs between factions, they may seek approval for a faction war from the Faction Team. This approval is sought when multiple Character Kills are expected and the goal is to debilitate or eradicate the opposing faction(s). Faction wars may be ended if all factions involved in the war agree to a truce or at any time the Faction Team deems necessary.

Character-Killing
-----------------
After a faction war is approved, every violent conflict is considered a character-kill situation for the faction members, associates, assistants and perceived members. Admin supervision should be requested wherever possible to prevent any disputes. Any character kills or confrontations regarding the faction war should be reported to the Faction Team so they may keep track of the feud.

Alternate Characters
--------------------
No alternate characters from the factions or players involved may be allowed at any time with the exception of approved leadership alts. New or low hour characters may not be created or used during the faction war.


Use of Government Perks
=======================
Any faction that is financially supported through the Faction Team or Scripting may not have a wage higher than $1,500 except for brief periods of time where bonuses may be given out for the holidays.

Corruption
==========
Corruption in governmnet factions is restricted unless a set of parameters is created and submitted to the Faction Team for approval. After approval, the faction leadership may delegate according to the parameters who has corruption. 

Financial corruption such as embezzling money is not allowed under any circumstances unless FT leadership & UAT approval is given.

Faction Recruitment
===================
In the event someone is not able to continue roleplaying in their faction permanently, such as being imprisoned for life or killed, they must wait at least 72 hours before rejoining the faction in any capacity. If the faction is rejoined the player must wait 14 days at minimum before attaining one rank below the previously held rank.

Exceptions for this may be made by contacting the Faction Team, especially regarding faction leadership ranks.

Alternate Characters
====================
Faction leaders may obtain permission from the Faction Team to have a single alternate character in their faction at a time. This character may not hold any supervisory position.

Faction Shutdown
================
In the event that a faction, both legal or illegal succumb to inactivity, all faction leaders must agree on the same plan of action if the following events occur;

* Any exterior mapping is modified or deleted.
* Any interior is OOCly deleted or modified.
* Any faction asset such as a gun NPC is deleted.
* A voluntary shutdown is set into motion.




